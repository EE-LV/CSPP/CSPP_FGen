CS++ FGen README
================
This is the function generator package for CS++. It contains source code for all kind of different function generators so far integrated within CS++. Note that all function generators still need additional drivers installed.

All function generator actors have inherited from the *CSPP_FGen.lvclass* base actor. They can use the base GUI for function generators *CSPP_FGenGUI.lvclass*.

![An example GUI for the operation of a function generator within CS++](Docs/fgengui_example.png)

| function generator class name | type | device driver | additional Ini-file entries |
| ----------------------------- | ---- | ------------- | --------------------------- |
| Agilent33XXX                  | driver | [Agilent33XXX](https://git.gsi.de/EE-LV/Drivers/Agilent33XXX) | none |
| Agilent3352X*                 | driver | [Agilent3352X](https://git.gsi.de/EE-LV/Drivers/Agilent3352X) | none |

* Note that this driver does not support USB devices. It somehow works but shows some unintendent behavior. In particular it is not possible to restart an instance for a USB connected device, since the initialisation routine
freezes. The device has to be switched off and on again. This is a bug within the Agilent usage of the NI VISA driver. It can probably be solved by the usage of the [Agilent driver](https://www.keysight.com/main/software.jspx?cc=DE&lc=ger&ckey=1937336&nid=-536902324.940638.02&id=1937336).
A disussion of this problem can be found [here](https://forums.ni.com/t5/LabVIEW/Keysight-Waveform-generator-33521B-hangs-up/td-p/3724435/page/2?profile.language=en).

A section describing the configuration Ini-file for frequency generators can be found within the readme file of the base actor class.

Required CS++ packages
----------------------

1. [**CSPP_Core**](https://git.gsi.de/EE-LV/CSPP/CSPP_Core): The core package is needed for all CS++ applications.
2. [**CSPP_DeviceBase**](https://git.gsi.de/EE-LV/CSPP/CSPP_DeviceBase): This package contains the base class for all frequency generators *CSPP_FGen.lvclass* and a basic
GUI *CSPP_FGenGUI.lvclass* from which all functions of the function generator can be addressed.

Optional CS++ packages
----------------------

1. [**CSPP_ListGUI**](https://git.gsi.de/EE-LV/CSPP/CSPP_ListGUI): The CS++ ListGUI is a GUI which can be used with all types of function generators. The design philosophy is to
control all channels of the function generator from an Excel-like list without the need of any buttons.

![An example of the ListGUI to be used to control a function generator](Docs/listgui_example.png)

Related information
---------------------------------
Author: H.Brand@gsi.de, D.Neidherr@gsi.de

Copyright 2020  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.